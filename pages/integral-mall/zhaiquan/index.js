var t = require("../../../api.js"), i = require("../../../wxParse/wxParse.js"),
  a = getApp(),
  n = !1,
  o = !1;

Page({
  data: {
    status: -1,
    show_channel: !1,
    channel: -1,
    channel_list: [],
    apply_pic_list: [],
    img_status: 0,
    showQiTa:false,
    name: undefined,
    phone: undefined,
    money: undefined,
    reason: undefined,
    qita:undefined,
    chooseImgStatus: 0,
    checkStatusN1:false,
    checkStatusN2:false,
    xieyi:undefined,
    xieyi_status:false,
    old_status:-1
  },
  onLoad: function(t) {
    a.pageOnLoad(this), n = !1, o = !1;
  },
  onReady: function() {},
  onShow: function() {
    getApp().pageOnShow(this);
    if (this.data.chooseImgStatus == 0) {
      this.loadData();
    }
  },
  onHide: function() {},
  onUnload: function() {},
  onPullDownRefresh: function() {},
  onReachBottom: function() {},
  loadData: function() {
    var e = this;

    wx.showLoading({
      title: "加载中"
    });
    a.request({
      url: t.zhaiquan.index,
      success: function(data) {
        if (data.status==2){
          data.status = 0
        }else if(data.status==1){
          if (data.img_status==1){
            data.status =9
          }
        }
        i.wxParse("content", "html", data.xieyi, e)
        e.setData({
          channel_list: data.channel_list,
          status: data.status,
          reason: data.reason,
        })
      },
      complete: function(t) {
        wx.hideLoading();
      }
    });

  },
  inputBlur: function(t) {
    var i = '{"' + t.currentTarget.dataset.name + '":"' + t.detail.value + '"}';
    this.setData(JSON.parse(i));
  },

  showChannel: function() {
    this.setData({
      show_channel: !0
    });
  },
  channelPicker: function(t) {
    var a = t.currentTarget.dataset.index;
    var qita = this.data.showQiTa;
    if (a.name=='其他'){
      qita = true
    }else{
      qita =false
    }
    this.setData({
      channel: a,
      show_channel: !1,
      showQiTa:qita
    });
  },
  payClose: function() {
    this.setData({
      show_channel: !1
    });
  },
  chooseImage: function(t) {
    var e = this,
      i = e.data.apply_pic_list,
      o = i.length;
    this.setData({
      chooseImgStatus: 1
    });
    wx.chooseImage({
      count: 10 - o,
      success: function(t) {
        i = i.concat(t.tempFilePaths), e.setData({
          apply_pic_list: i
        });
      }
    });
  },
  deleteImage: function(t) {
    var e = this,
      i = t.currentTarget.dataset.picIndex,
      o = e.data.apply_pic_list;
    o.splice(i, 1), e.setData({
      apply_pic_list: o
    });
  },

  zhaiquanSubmit: function() {
    var e = this,
      imgs = []
    if (!e.data.channel) {
      return void wx.showToast({
        title: "债权平台未选择",
        image: "/images/icon-warning.png"
      });
    }
    if (!e.data.name) {
      return void wx.showToast({
        title: "请填写债权人姓名",
        image: "/images/icon-warning.png"
      });
    }
    if (!/^\+?\d[\d -]{8,12}\d/.test(e.data.phone)) {
      return void wx.showToast({
        title: "手机号格式错误",
        image: "/images/icon-warning.png"
      });
    }
    if (!e.data.money) {
      return void wx.showToast({
        title: "请填写未收本金总金额",
        image: "/images/icon-warning.png"
      });
    }
    if (!this.data.checkStatusN1){
      return void wx.showToast({
        title: "请同意协议书",
        image: "/images/icon-warning.png"
      });
    }
    if (this.data.channel.name == '其他' && !this.data.qita){
      return void wx.showToast({
        title: "请填写平台名称",
        image: "/images/icon-warning.png"
      });
    }

    function submit() {
      a.request({
        url: t.zhaiquan.submit,
        method: "post",
        data: {
          channel_id: e.data.channel.id,
          qita: e.data.qita,
          name: e.data.name,
          phone: e.data.phone,
          money: e.data.money,
          img_status: e.data.img_status,
          imgs: JSON.stringify(imgs),
        },
        success: function(t) {
          wx.hideLoading(), 0 == t.code && wx.showModal({
            title: "提示",
            content: t.msg,
            showCancel: !1,
            success: function(e) {
              wx.redirectTo({
                url: "/pages/integral-mall/zhaiquan/index"
              });
            }
          }), 1 == t.code && wx.showToast({
            title: t.msg,
            image: "/images/icon-warning.png"
          });
        }
      });
    }
    if (this.data.img_status == 0) {
      if (this.data.apply_pic_list.length == 0) {
        return void wx.showToast({
          title: "请上传凭证",
          image: "/images/icon-warning.png"
        });
      }

      function up() {
        var a = 0;
        debugger

        for (var s in e.data.apply_pic_list) {
          wx.uploadFile({
            url: t.default.upload_image,
            name: "image",
            filePath: e.data.apply_pic_list[s],
            complete: function(t) {
              if (t.data) {
                var s = JSON.parse(t.data);
                0 == s.code && (imgs.push(s.data.url));
              }
              if (++a == e.data.apply_pic_list.length) {
                submit()
              };
            }
          });
        }
      }
      up();
    } else {
      if (!this.data.checkStatusN2){
        return void wx.showToast({
          title: "请同意授权",
          image: "/images/icon-warning.png"
        });
      }
      submit()
    }

  },
  chongxin: function() {
    this.setData({
      status: 0
    })
  },
  checkStatus1:function(){
    this.setData({
      checkStatusN1: !this.data.checkStatusN1
    })
  },
  checkStatus2:function(){
    this.setData({
      checkStatusN2: !this.data.checkStatusN2
    })
  },
  changeImgStatus:function(){
    this.setData({
      img_status: 1
    })
  },
  showxieyi:function(){
    var dd = this.data.status;
    var old = this.data.old_status;
    if (!this.data.xieyi_status){
      old = dd;
      dd= 99
    }else{
      dd = old
    }
    this.setData({
      status:dd,
      old_status: old,
      xieyi_status: !this.data.xieyi_status
    })
  }

});