var e = require("../../api.js"), a = getApp();

Page({
  data: {
    address_list: null
  },
  onLoad: function (e) {
    a.pageOnLoad(this);
  },
  onReady: function () { },
  onShow: function () {
  },
  zhaiquanUser:function(){
    wx.redirectTo({
      url: '../integral-mall/zhaiquan/index'
    });
  },
  feizhaiquanUser:function(){
    wx.redirectTo({
      url: '../user/user'
    });
  }
});